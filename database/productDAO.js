const connection = require("./connectionDAO");

let operations = {
  getByUUID: function (productUUID) {
    return connection
      .promise()
      .query("SELECT * FROM product WHERE uuid = ?", [productUUID]);
  },

  search: function (name) {
    return connection
      .promise()
      .query("SELECT * FROM product WHERE name LIKE ?", ["%" + name + "%"]);
  },

  list: function () {
    return connection.promise().query("SELECT * FROM product");
  },

  save: function (product) {
    return connection
      .promise()
      .execute(
        "INSERT INTO product(name, description, uuid, price, size, category) VALUES(?, ?, ?, ?, ?, ?)",
        [
          product.name,
          product.description,
          product.uuid,
          product.price,
          product.size,
          product.category,
        ]
      );
  },

  remove: function (productUUID) {
    return connection
      .promise()
      .execute("DELETE FROM product WHERE uuid = ?", [productUUID]);
  },

  update: function (product) {
    return connection
      .promise()
      .execute(
        "UPDATE product SET name=?, description=?, price=?, size=?, category=? WHERE uuid=?",
        [
          product.name,
          product.description,
          product.price,
          product.size,
          product.category,
          product.uuid,
        ]
      );
  },
};

module.exports = operations;
