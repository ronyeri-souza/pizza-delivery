const connection = require("./connectionDAO");

let operations = {
  getByUUID: function (userUUID) {
    return connection
      .promise()
      .query("SELECT * FROM user WHERE uuid = ?", [userUUID]);
  },

  getByEmail: function (email) {
    return connection
      .promise()
      .query("SELECT * FROM user WHERE email = ?", [email]);
  },

  create: function (user) {
    return connection
      .promise()
      .execute(
        "INSERT INTO user(name, email, password, phone, uuid, address, number, complement, district, city) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        [
          user.name,
          user.email,
          user.password,
          user.phone,
          user.uuid,
          user.address,
          user.number,
          user.complement,
          user.district,
          user.city,
        ]
      );
  },
  
  update: function (user) {
    return connection
      .promise()
      .execute(
        "update user set name=?, email=?, phone=?, uuid=?, address=?, number=?, complement=?, district=?, city=? where id=?",
        [
          user.name,
          user.email,
          user.phone,
          user.uuid,
          user.address,
          user.number,
          user.complement,
          user.district,
          user.city,
          user.id
        ]
      );
  }

  
};

module.exports = operations;
