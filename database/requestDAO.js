const connection = require("./connectionDAO");

let operations = {
  getUserRequests: function (userUUID) {
    return connection
      .promise()
      .query(
        "SELECT * FROM request WHERE user_id IN (SELECT id FROM user WHERE uuid = ?) ORDER BY id DESC",
        [userUUID]
      );
  },

  getUserRequestItemsForHistory: function (userUUID, requestId) {
    return connection
      .promise()
      .query(
        "SELECT c.id, c.uuid AS 'itemUUID', c.user_id, u.uuid AS 'userUUID', p.name, p.description, c.current_price, p.size, c.amount, c.observations FROM product p INNER JOIN cart_item c ON p.id = c.product_id INNER JOIN user u ON c.user_id = u.id WHERE c.user_id IN (SELECT u.id FROM user u WHERE u.uuid = ?) AND c.request_id = ?",
        [userUUID, requestId]
      );
  },
};

module.exports = operations;
