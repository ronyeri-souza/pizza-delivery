const connection = require("./connectionDAO");

let operations = {
  getUserCartItems: function (userUUID) {
    return connection
      .promise()
      .query(
        "SELECT c.id, c.uuid AS 'itemUUID', c.user_id, u.uuid AS 'userUUID', p.name, p.description, p.price, p.size, c.amount, c.observations FROM product p INNER JOIN cart_item c ON p.id = c.product_id INNER JOIN user u ON c.user_id = u.id WHERE c.deleted = ? AND c.user_id IN (SELECT u.id FROM user u WHERE u.uuid = ?)",
        [false, userUUID]
      );
  },

  addItemsOnCart: function (cart) {
    return connection
      .promise()
      .execute(
        "INSERT INTO cart_item(uuid, product_id, user_id, amount, observations, deleted, current_price) VALUES(?, ?, ?, ?, ?, ?, ?)",
        [cart.uuid, cart.product_id, cart.user_id, cart.amount, cart.observations, false, cart.current_price]
      );
  },

  removeItemFromCart: function (itemUUID, userUUID) {
    return connection
      .promise()
      .execute("DELETE FROM cart_item WHERE uuid = ? AND user_id IN (SELECT id FROM user WHERE uuid = ?)", [itemUUID, userUUID]);
  },

  removeAllItemsFromCart: function(userUUID) {
    return connection.promise().execute("DELETE FROM cart_item WHERE request_id IS NULL AND user_id IN (SELECT id FROM user WHERE uuid = ?)", [userUUID]);
  },

  updateCartRequest: function (requestId, userUUID) {
    return connection
      .promise()
      .execute(
        "UPDATE cart_item SET request_id = ?, deleted = ? WHERE user_id IN (SELECT id FROM user WHERE uuid = ?) AND request_id IS NULL",
        [requestId, true, userUUID]
      );
  },

  updateCartItems: function (cartItem) {
    return connection
      .promise()
      .execute(
        "UPDATE cart_item SET amount = ?, observations = ? WHERE uuid = ?",
        [cartItem.amount, cartItem.observations, cartItem.itemUUID]
      );
  },

  createRequest: function (request) {
    return connection
      .promise()
      .execute("INSERT INTO request(uuid, user_id, subtotal, total, request_date) VALUES(?, ?, ?, ?, ?)", [
        request.uuid,
        request.user_id,
        request.subtotal,
        request.total,
        request.request_date,
      ]);
  },
};

module.exports = operations;
