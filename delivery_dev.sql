-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: delivery_dev
-- ------------------------------------------------------
-- Server version	5.5.5-10.6.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cart_item`
--

DROP TABLE IF EXISTS `cart_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `observations` text DEFAULT NULL,
  `current_price` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  KEY `request_id` (`request_id`),
  CONSTRAINT `cart_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `cart_item_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `cart_item_ibfk_3` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_item`
--

LOCK TABLES `cart_item` WRITE;
/*!40000 ALTER TABLE `cart_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(4,2) NOT NULL,
  `size` varchar(10) DEFAULT NULL,
  `uuid` varchar(255) NOT NULL,
  `imageUrl` varchar(255) DEFAULT NULL,
  `category` varchar(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (5,'Pizza Calabresa','Queijo, molho de tomate, calabresa em rodelas, cebola, tomate picado, azeite e orégano.',18.00,'Pequena','8270550a-084d-40db-ade8-62f940b529bf',NULL,'Pizzas'),(6,'Pizza Calabresa','Queijo, molho de tomate, calabresa em rodelas, cebola, tomate picado, azeite e orégano.',22.00,'Média','4b2d3bb7-1e8b-4996-b074-ccbd515c023c',NULL,'Pizzas'),(7,'Pizza Calabresa','Queijo, molho de tomate, calabresa em rodelas, cebola, tomate picado, azeite e orégano.',28.00,'Grande','224d2a93-5c94-4c71-95c0-f454a9c64cc0',NULL,'Pizzas'),(8,'Pizza Portuguesa','Queijo, azeitona verde ou preta, ovo cozido, presunto cozido, cebola, ervilha, molho de tomate e azeite.',22.00,'Pequena','ac380b5d-5a02-4ff3-b9b8-b211856ba4e9',NULL,'Pizzas'),(9,'Pizza Portuguesa','Queijo, azeitona verde ou preta, ovo cozido, presunto cozido, cebola, ervilha, molho de tomate e azeite.',25.00,'Média','1c57aa50-fb8b-4b26-9412-82baa8e18261',NULL,'Pizzas'),(10,'Pizza Portuguesa','Queijo, azeitona verde ou preta, ovo cozido, presunto cozido, cebola, ervilha, molho de tomate e azeite.',32.00,'Grande','4c46618f-2876-4f34-89fa-deae7d00e96a',NULL,'Pizzas'),(11,'Pizza Mussarela','Queijo mussarela em abundância, molho de tomate fresco, azeitona, rodelas de tomate e orégano',16.00,'Pequena','2a70c872-b1d3-416a-83fa-2ab620ec8c18',NULL,'Pizzas'),(12,'Pizza Mussarela','Queijo mussarela em abundância, molho de tomate fresco, azeitona, rodelas de tomate e orégano',20.00,'Média','cbd56104-b456-419d-bdf9-7e530110f446',NULL,'Pizzas'),(13,'Pizza Mussarela','Queijo mussarela em abundância, molho de tomate fresco, azeitona, rodelas de tomate e orégano',25.00,'Grande','4ceed9a2-c73d-4437-a7af-7a10ea2fb0b8',NULL,'Pizzas'),(14,'Pizza Marguerita','Molho de tomate, manjericão, rodelas de tomate fresco, azeitona, queijo mussarela e orégano salpicado.',25.00,'Pequena','ab9bd442-5c83-4aab-85c3-5815d528cbb8',NULL,'Pizzas'),(15,'Pizza Marguerita','Molho de tomate, manjericão, rodelas de tomate fresco, azeitona, queijo mussarela e orégano salpicado.',30.00,'Média','84b042c4-800b-4634-9166-057e5359c940',NULL,'Pizzas'),(16,'Pizza Marguerita','Molho de tomate, manjericão, rodelas de tomate fresco, azeitona, queijo mussarela e orégano salpicado.',35.00,'Grande','12693815-f8f9-4944-b174-50c88eedf78e',NULL,'Pizzas'),(17,'Pizza 4 Queijos','Quatro queijos diferentes, como mussarela, gorgonzola, parmesão e catupiry.',25.00,'Pequena','3aa6ca6e-8eb5-42a4-b25c-8a70aa31b9f2',NULL,'Pizzas'),(18,'Pizza 4 Queijos','Quatro queijos diferentes, como mussarela, gorgonzola, parmesão e catupiry.',30.00,'Média','339c70f5-2a22-48c8-9411-1fd6dae4658f',NULL,'Pizzas'),(19,'Pizza 4 Queijos','Quatro queijos diferentes, como mussarela, gorgonzola, parmesão e catupiry.',36.00,'Grande','a920a255-387a-471c-884a-02933e7a4fe8',NULL,'Pizzas'),(20,'Pizza Frango com Catupiry','Queijo mussarela, frango, catupiry, sálvia e molho de tomate.',25.00,'Pequena','05ef07c0-a4da-4519-8748-e3e68def44bc',NULL,'Pizzas'),(21,'Pizza Frango com Catupiry','Queijo mussarela, frango, catupiry, sálvia e molho de tomate.',30.00,'Média','c4db2c09-bdcc-4915-9eae-2ae47ee8e475',NULL,'Pizzas'),(22,'Pizza Frango com Catupiry','Queijo mussarela, frango, catupiry, sálvia e molho de tomate.',35.00,'Grande','54adf532-4c1e-4c85-992e-3c05be9749d4',NULL,'Pizzas'),(23,'Pizza Chocolate Mesclado','Chocolate ao leite e chocolate branco. ',25.00,'Pequena','1181e1b9-f0f2-4fe1-876b-acf486de6177',NULL,'Pizzas Doces'),(24,'Pizza Chocolate Mesclado','Chocolate ao leite e chocolate branco. ',30.00,'Média','ce0c1b88-5522-4222-b194-cdf852f62f6e',NULL,'Pizzas Doces'),(25,'Pizza Chocolate Mesclado','Chocolate ao leite e chocolate branco. ',35.00,'Grande','dbe022fd-b9b0-4c86-a7b4-9b5a4882d13c',NULL,'Pizzas Doces'),(26,'Pizza Brigadeiro','Chocolate ao leite, granulados e morangos.',30.00,'Pequena','11364b62-707f-4fbb-99c4-2bd77c969409',NULL,'Pizzas Doces'),(27,'Pizza Brigadeiro','Chocolate ao leite, granulados e morangos.',35.00,'Média','c0a24e14-3957-485f-b9cf-c31f1a4c8f4e',NULL,'Pizzas Doces'),(28,'Pizza Brigadeiro','Chocolate ao leite, granulados e morangos.',40.00,'Grande','93d2a90a-162b-4f5c-aded-cde4e5cbec79',NULL,'Pizzas Doces'),(29,'Pizza Ganache com Rum','Chocolate ao leite, creme de leite e rum.',32.00,'Pequena','00f2f58e-4b10-46e5-8d0e-ca0be1084b24',NULL,'Pizzas Doces'),(30,'Pizza Ganache com Rum','Chocolate ao leite, creme de leite e rum.',38.00,'Média','168b27b7-a100-4a5a-aad4-5425e582947f',NULL,'Pizzas Doces'),(31,'Pizza Ganache com Rum','Chocolate ao leite, creme de leite e rum.',40.00,'Grande','e087c890-9bc7-457f-952d-b6c87ad4401c',NULL,'Pizzas Doces'),(32,'Lasanha à Bolonhesa','Cebola, alho, orégano, sal, pimenta-do-reino, carne moída, molho de tomate, cheiro-verde, massa de lasanha, presunto e queijo',40.00,NULL,'733133ad-104f-4d0d-84cf-145f938ba223',NULL,'Massas'),(33,'Espaguete à Bolonhesa','Cenoura, espaguete, vinho branco seco, aipo e leite.',40.00,NULL,'b7971542-78f1-4df9-8c9e-17b5ae1b18b5',NULL,'Massas'),(34,'Mousse de Limão','Leite condensado, suco de limão e raspas de limão.',12.00,NULL,'84bec95d-6e82-44f0-a4fc-86c34fb42dba',NULL,'Sobremesas'),(35,'Torta de Limão com Chocolate','Biscoito maizena, suco de limão e chocolate meio amrago.',20.00,NULL,'2e41b7b9-08cd-4c98-a9c7-3eee48867f9a',NULL,'Sobremesas'),(36,'Pavê Simples de Ninho','Leite ninho, chocolate branco ralado, biscoito maizena e morangos.',20.00,NULL,'d54cc9cf-3703-4ab7-b2ff-1ecbf6508989',NULL,'Sobremesas'),(37,'Pavê Simples de Limão','Biscoito maizena, chá de limão e raspas de limão.',15.00,NULL,'4951bc13-cdd2-4395-8348-a43cc68d067a',NULL,'Sobremesas'),(38,'Delícia Gelada de Abacaxi','Abacaxi em calda escorrido, licor e suco de limão.',15.00,NULL,'a7e907c8-220f-4038-9470-6ea92f478a9b',NULL,'Sobremesas'),(39,'Crepe de Nutella com Leite Ninho','Creme de cacau, avelã e brigadeiro de leite ninho.',15.00,NULL,'45585876-f406-4ce4-889b-e56de4e882f4',NULL,'Crepes'),(40,'Crepe de Frango','Frango cozido desfiado, tomates sem sementes picados, champignon picado e requeijão cremoso.',22.00,NULL,'0a09889f-4061-4792-8de7-e10877759618',NULL,'Crepes'),(41,'Crepe de Presunto e Queijo','Queijo mussarela, presunto e queijo ralado.',12.00,NULL,'c0fa10cb-6c40-4365-a281-d52139be6a30',NULL,'Crepes'),(42,'Coca-cola 1L','Refrigerante coca-cola pet 1L',6.00,NULL,'b3d6e71c-6be2-41c2-b6a4-9eaaf2bcb96e',NULL,'Bebidas'),(43,'Água Mineral Indaiá 500ml','Água mineral sem gás Indaiá 500ml',3.00,NULL,'f6c40593-5a4f-4902-9950-d48a427826c6',NULL,'Bebidas'),(44,'Kuat Guaraná 2L','Refrigerante Kuat guaraná pet 2L',5.00,NULL,'71cd3618-8573-439c-8715-9189ed7b4cee',NULL,'Bebidas');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `request_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request`
--

LOCK TABLES `request` WRITE;
/*!40000 ALTER TABLE `request` DISABLE KEYS */;
/*!40000 ALTER TABLE `request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `number` int(10) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `district` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-25 18:22:24
