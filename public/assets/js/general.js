function activeMenu(ativo){
    let links = document.getElementsByClassName('menu-link')
    for(link of links){
        if(link.classList.contains('active')){
            link.classList.remove('active');
        };
    }
    ativo.classList.add('active');
}


function openNav() {
    document.getElementById("mySidenav").style.width = "fit-content";
}
  
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}


// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
function ativaModal(id) {
    var modal = document.getElementById(id);
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
function fechaModal(id) {
    var modal = document.getElementById(id);
    modal.style.display = "none";
}

function add(id){
    let field = document.getElementById(id)
    field.value++;
    if(field.value > 1){
        document.getElementById("button-addon1").disabled = false;
    }
}

function remove(id){
    let field = document.getElementById(id)
    if(field.value > 1){
        field.value--;
    }else{
        document.getElementById("button-addon1").disabled = true;
    }
}