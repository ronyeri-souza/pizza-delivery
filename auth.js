const bcrypt = require("bcryptjs");
const LocalStrategy = require("passport-local").Strategy;
const userDAO = require("./database/userDAO");

module.exports = function (passport) {
  passport.serializeUser((user, done) => {
    done(null, user.uuid);
  });

  passport.deserializeUser((userUUID, done) => {
    try {
      userDAO.getByUUID(userUUID).then(([rows]) => {
        done(null, rows[0]);
      });
    } catch (err) {
      done(err, null);
    }
  });

  passport.use(
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
      },
      (email, password, done) => {
        try {
          userDAO.getByEmail(email).then(([rows]) => {
            if (!rows[0]) {
              return done(null, false);
            }
            const isValid = bcrypt.compareSync(password, rows[0].password);
            if (!isValid) return done(null, false);
            return done(null, rows[0]);
          });
        } catch (err) {
          done(err, false);
        }
      }
    )
  );
};
