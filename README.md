# Pizzaria Hot Tomatoes


### Descrição do projeto

Esse projeto foi desenvolvido como parte da avaliação prática da disciplina Desenvolvimento para Web III, do curso Tecnólogo em Análise e Desenvolvimento de Sistemas - IFPE.

O sistema consiste na realização de pedidos em uma pizzaria, onde o usuário pode se cadastrar, realizar login, adicionar itens ao carrinho e observações sobre seu pedido. Após realizar os pedidos o usuário pode ver seu histórico de pedidos e detalhes destes pedidos, como os itens que foram pedidos.


### Tecnologias, pacotes e etc.

Foi utilizado o Node.js em conjunto com o template engine EJS para criação das páginas da aplicação.

Dentre os pacotes utilizados, estão:
- express
- bcrypt
- express-session
- passport
- passport-local
- mysql2
- uuid
- nodemon - Para não precisar reiniciar o servidor manualmente a cada alteração.

### Instalando o projeto e usando-o

1. Antes de começar, abaixo estão as versões:

- npm - v6.14.11
- node.js - v14.15.5
- bcrypt - v2.4.3
- express - v4.16.1
- express-session - v1.17.2
- moment - v2.29.1
- nodemon - v2.0.15
- mysql2 - v2.3.3
- passport - v0.5.2
- passport-local - v1.0.0
- uuid - v.8.3.2


2. Use o comando `npm install` para instalar os pacotes requeridos.

3. Agora, deve ser importado o dump do banco de dados disponibilizado nos arquivos, com nome `delivery_dev.sql`.

    3.1. Depois de ter o dump na máquina (que já vem com produtos cadastrados), deve ser configurada a conexão do projeto com o banco, indo no diretório `database > connectionDAO.js` e ajustando o host, username e password para os da sua máquina.












