var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var flash = require("express-flash");
var passport = require("passport");
var session = require("express-session");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

require("./auth")(passport);
app.use(
  session({
    secret: "a2c5cd18-253f-4c18-864c-9d367e2befa3",
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 30 * 60 * 1000 }, //30min
  })
);
app.use(passport.initialize());
app.use(passport.session());

function authenticationMiddleware(request, response, next) {
  if (request.isAuthenticated()) return next();
  response.redirect("/login?fail=true");
}

app.use(flash());

app.use(express.static(path.join(__dirname, "public")));

var indexRouter = require("./routes/index");
var loginRouter = require("./routes/login");
var registerRouter = require("./routes/register");
var homeRouter = require("./routes/home");
var cartRouter = require("./routes/cart");
var requestRouter = require('./routes/request');

app.use("/", indexRouter);
app.use("/login", loginRouter);
app.use("/register", registerRouter);
app.use("/home", authenticationMiddleware, homeRouter);
app.use("/cart", authenticationMiddleware, cartRouter);
app.use("/requests", authenticationMiddleware, requestRouter);
app.use("/public", express.static("public"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
