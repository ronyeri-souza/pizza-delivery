const express = require("express");
const router = express.Router();
const userDao = require("../database/userDAO");
const bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require("uuid");

router.get("/", async function (request, response) {

  let row = {
    id: '',
    name: '',
    email: '',
    password: '',
    phone: '',
    uuid: '',
    address: '',
    number: '',
    complement: '',
    district: '',
    city: ''
  }
  if (request.query.id){
    [result] = await userDao.getByUUID(request.query.id)
    row = result[0];
  }
  response.render("authentication/register" , {user: row});
  
});

router.post("/", function (request, response) {

  if(request.body.uuid){
    operator = userDao.update;
    success = "Edição realizada com sucesso"
    error = "Houve um erro no edição dos seus dados!"
    endRoute = "/home"
  }else{
    var hashPassword = bcrypt.hashSync(request.body.password, 10);
    request.body.password = hashPassword;
    request.body.uuid = uuidv4();
    success = "Sua conta foi cadastrada com sucesso e você já pode fazer login!"
    error = "Houve um erro no seu cadastro!"
    operator = userDao.create;
    endRoute = "/login"
  }

  operator(request.body)
    .then(([result]) => {
      request.flash("success",success);
      response.redirect(endRoute);
    })
    .catch((err) => {
      console.log(err);
      request.flash("error", error);
      response.redirect("/register");
    });
});

module.exports = router;
