var express = require("express");
var router = express.Router();
var productDAO = require("../database/productDAO");

router.get("/", function (request, response, next) {
  productDAO
    .list()
    .then(([rows]) => {
      response.render("home", { products: rows, user: request.user });
    })
    .catch((err) => {
      console.log(err);
      response.render("home", { products: [], user: request.user });
    });
});

module.exports = router;
