var express = require("express");
var router = express.Router();
var cartDAO = require("../database/cartDAO");
const { v4: uuidv4 } = require("uuid");

router.get("/", function (request, response) {
  cartDAO
    .getUserCartItems(request.query.user)
    .then(([rows]) => {
      let total = 0.0;
      let subtotal = 0.0;
      let delivery = 5.5;
      rows.forEach((item) => {
        subtotal += parseFloat(item.price) * item.amount;
      });
      if (rows.length > 0) {
        var user_id = rows[0].user_id;
        total = subtotal + delivery;
      }
      response.render("cart", {
        items: rows,
        subtotal: subtotal.toFixed(2),
        total: total.toFixed(2),
        delivery: delivery.toFixed(2),
        userUUID: request.query.user,
        user_id: user_id,
      });
    })
    .catch((err) => {
      console.log(err);
      response.render("cart", { items: [] });
    });
});

router.post("/add", function (request, response) {
  request.body.uuid = uuidv4();
  cartDAO
    .addItemsOnCart(request.body)
    .then(([result]) => {
      request.flash("success", "O item foi adicionado ao carrinho!");
      response.redirect("/home");
    })
    .catch((err) => {
      console.log(err);
      request.flash("error", "Erro ao adicionar produto ao carrinho!");
      response.redirect("/home");
    });
});

router.post("/update", function (request, response) {
  cartDAO
    .updateCartItems(request.body)
    .then(([result]) => {
      request.flash("success", "Item editado com sucesso!");
      response.redirect(`/cart?user=${request.body.userUUID}`);
    })
    .catch((err) => {
      console.log(err);
      request.flash("error", "Houve um erro na edição do seu item.");
      response.redirect(`/cart?user=${request.body.userUUID}`);
    });
});

router.post("/remove", function (request, response) {
  cartDAO
    .removeItemFromCart(request.body.itemUUID, request.body.userUUID)
    .then(([result]) => {
      request.flash("success", "O item foi removido do carrinho!");
      response.redirect(`/cart?user=${request.body.userUUID}`);
    })
    .catch((err) => {
      console.log(err);
      request.flash("error", "Houve um erro na remoção do item do carrinho.");
      response.redirect(`/cart?user=${request.body.userUUID}`);
    });
});

router.post("/removeAll", function (request, response) {
  cartDAO
    .removeAllItemsFromCart(request.body.userUUID)
    .then(([result]) => {
      request.flash(
        "success",
        "Todos os itens do seu carrinho foram removidos!"
      );
      response.redirect(`/cart?user=${request.body.userUUID}`);
    })
    .catch((err) => {
      console.log(err);
      request.flash("error", "Houve um erro na remoção dos itens do carrinho.");
      response.redirect(`/cart?user=${request.body.userUUID}`);
    });
});

router.post("/request", function (request, response) {
  request.body.uuid = uuidv4();
  request.body.request_date = new Date();
  cartDAO
    .createRequest(request.body)
    .then(([result]) => {
      if (result.affectedRows > 0) {
        cartDAO
          .updateCartRequest(result.insertId, request.body.userUUID)
          .then(([result]) => {
            request.flash(
              "success",
              "Obrigado por pedir conosco! Seu pedido já está sendo preparado e logo sairá para a entrega!"
            );
            response.redirect("/home");
          })
          .catch((err) => {
            console.log(err);
            response.redirect("/home");
          });
      }
    })
    .catch((err) => {
      console.log(err);
      request.flash("error", "Houve um erro no processamento do seu pedido!");
      response.redirect("/home");
    });
});

module.exports = router;
