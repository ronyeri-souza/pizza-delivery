const express = require("express");
const router = express.Router();
const passport = require("passport");

router.get("/", function (request, response, next) {
  if (request.query.fail) {
    response.render('authentication/login', {
      message: 'O usuário e/ou senha digitados estão incorretos!',
    });
  } else {
    response.render("authentication/login", { message: null });
  }
});

router.post(
  "/",
  passport.authenticate("local", {
    successRedirect: "/home",
    failureRedirect: "/login?fail=true"
  })
);

router.post('/logout', function (request, response) {
  request.logout();
  response.redirect('/login');
});


module.exports = router;
