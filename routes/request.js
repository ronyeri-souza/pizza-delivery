var express = require("express");
var router = express.Router();
var requestDAO = require("../database/requestDAO");
const { v4: uuidv4 } = require("uuid");
const moment = require("moment");

router.get("/", function (request, response) {
  requestDAO
    .getUserRequests(request.query.user)
    .then(([rows]) => {
      if (rows.length > 0) {
        moment.locale("pt-br");
        rows.forEach((row) => {
          row.request_date = moment(row.request_date).format("LLL");
        });
      }
      response.render("requests/requests", {
        requests: rows,
        userUUID: request.query.user,
      });
    })
    .catch((err) => {
      response.render("requests/requests", { requests: [] });
    });
});

router.get("/details", function (request, response) {
  requestDAO
  .getUserRequestItemsForHistory(request.query.userUUID, request.query.request_id)
  .then(([rows]) => {
    response.render("requests/request-details", {
      requestItems: rows,
      userUUID: request.query.userUUID,
      requestId: request.query.request_id,
    });
  })
  .catch((err) => {
    console.log(err);
    response.render("requests/request-details", { requestItems: [] });
  });
});

module.exports = router;
