var express = require('express');
var router = express.Router();
var productDAO = require('../database/productDAO');

router.get('/', function (request, response) {
  productDAO
    .list()
    .then(([rows]) => {
      response.render('index', { products: rows });
    })
    .catch((err) => {
      console.log(err);
      response.render('index', { products: [] });
    });
});

module.exports = router;
